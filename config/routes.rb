Rails.application.routes.draw do
  root 'hone#index'

  resources :posts do
  	resources :activities
  end
  devise_for :users, controllers: {registration: "registration"}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
