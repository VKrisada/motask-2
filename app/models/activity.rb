class Activity < ActiveRecord::Base
	belongs_to :post
	accepts_nested_attributes_for :post
	validates_presence_of :hobby
end
