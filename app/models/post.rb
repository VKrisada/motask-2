class Post < ActiveRecord::Base
	belongs_to :user
	has_many :activitys
	accepts_nested_attributes_for :activitys
	validates_presence_of :title
end